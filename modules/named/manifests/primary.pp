class named::primary inherits named::authoritative {
	include dnsextras::entries

	@ferm::rule { '01-dsa-bind-4':
		domain      => '(ip ip6)',
		description => 'Allow nameserver access',
		rule        => '&TCP_UDP_SERVICE_RANGE(53, ( $HOST_DNS_GEO $HOST_NAGIOS $HOST_RCODE0 $HOST_EASYDNS $HOST_NETNOD ) )',
	}

	concat::fragment { 'dsa-named-conf-puppet-misc---local-shared-keys':
		target => '/etc/bind/named.conf.puppet-misc',
		order  => '020',
		content  => @(EOF),
			include "/etc/bind/named.conf.shared-keys";
			| EOF
	}
	concat::fragment { 'dsa-named-conf-puppet-misc---named.conf.external-secondaries-ACLs':
		target => '/etc/bind/named.conf.puppet-misc',
		order  => '025',
		content => template('named/named.conf.external-secondaries-ACLs.erb'),
	}

	concat::fragment { 'dsa-named-conf-puppet-misc---openpgpkey-zone':
		target => '/etc/bind/named.conf.puppet-misc',
		order  => '020',
		content  => @("EOF"/$)
			// MAINTAIN-KEY: _openpgpkey.debian.org

			zone "_openpgpkey.debian.org" {
				type slave;
				file "db._openpgpkey.debian.org";
				allow-query { any; };
				masters {
					${ join(getfromhash($site::allnodeinfo, 'kaufmann.debian.org', 'ipHostNumber'), ";") } ;
				};
				allow-transfer {
					127.0.0.1;
					rcode0-ACL;
					dnsnode-ACL;
					dnsnodeapi-ACL;
				};
				also-notify {
					rcode0-masters;
					dnsnode-masters;
					dnsnodeapi-masters;
				};

				key-directory "/srv/dns.debian.org/var/keys/_openpgpkey.debian.org";
				sig-validity-interval 40 25;
				auto-dnssec maintain;
				inline-signing yes;
			};
			| EOF
	}

	concat::fragment { 'dsa-puppet-stuff--nsec3':
		target => '/etc/cron.d/dsa-puppet-stuff',
		content  => @(EOF)
			13 19 4 * * root chronic /usr/sbin/rndc signing -nsec3param 1 0 16 $(head -c 20 /dev/urandom | sha512sum | cut -b 1-10) debian.net
			29 12 7 * * root chronic /usr/sbin/rndc signing -nsec3param 1 0 16 $(head -c 20 /dev/urandom | sha512sum | cut -b 1-10) debian.org
			32 12 7 * * root chronic /usr/sbin/rndc signing -nsec3param 1 0 16 $(head -c 20 /dev/urandom | sha512sum | cut -b 1-10) debconf.org
			36 12 7 * * root chronic /usr/sbin/rndc signing -nsec3param 1 0 16 $(head -c 20 /dev/urandom | sha512sum | cut -b 1-10) _openpgpkey.debian.org

			| EOF
	}

}
