class debian_org::apt_restricted {
	site::aptrepo { 'db.debian.org.restricted':
		url        => 'https://db.debian.org/debian-admin',
		suite      => "${::lsbdistcodename}-restricted",
		components => 'non-free',
	}
}
