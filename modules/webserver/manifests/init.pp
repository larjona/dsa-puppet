#
class webserver {
	include webserver::defaultpage

	file { '/etc/cron.d/puppet-export-scheduled-shutdown': ensure => absent, }
	concat::fragment { 'dsa-puppet-stuff--webserver-export-shutdown':
		target => '/etc/cron.d/dsa-puppet-stuff',
		content  => @(EOF)
			*/2 * * * * root mkdir -p /run/dsa/shutdown-marker; if dsa-is-shutdown-scheduled; then echo 'system-in-shutdown' > /run/dsa/shutdown-marker/shutdown-in-progress; else rm -f /run/dsa/shutdown-marker/shutdown-in-progress; fi
			| EOF
	}
}
