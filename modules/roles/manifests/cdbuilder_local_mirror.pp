class roles::cdbuilder_local_mirror {
	include apache2

	$apache_addr = "172.29.103.1"
	$vhost_listen = "${apache_addr}:80"

	apache2::site { '010-local-mirror.cdbuilder.debian.org':
		site   => 'local-mirror.cdbuilder.debian.org',
		content => template('roles/apache-local-mirror.cdbuilder.debian.org.erb'),
	}

	file { "/etc/apache2/ports.conf":
		require => Package['apache2'],
		content  => @("EOF"),
				# This file is maintained by puppet
				Listen 127.0.0.1:80
				Listen $vhost_listen
				| EOF
		notify  => Service['apache2']
	}
}
