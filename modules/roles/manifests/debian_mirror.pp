class roles::debian_mirror {
	include roles::archvsync_base

	$mirror_basedir_prefix = hiera('role_config__mirrors.mirror_basedir_prefix')
	$archive_root = "${mirror_basedir_prefix}debian"
	$listen_addresses = hiera('roles.debian_mirror')
		.dig($::fqdn, 'listen-addresses')
		.then |$a| { $a + [ '127.0.0.1:80', '[::1]:80' ] }
		.lest || { ['*:80'] }

	$vhost_listen = join([
		*$listen_addresses,
		has_role('bgp') ? {
			true => '193.31.7.2:80 [2a02:158:ffff:deb::2]:80',
			default => '',
		}], ' ')

	apache2::site { '010-ftp.debian.org':
		site   => 'ftp.debian.org',
		content => template('roles/apache-ftp.debian.org.erb'),
	}

	$onion_v4_addr = hiera("roles.debian_mirror", {})
		.dig($::fqdn, 'onion_v4_address')
	if $onion_v4_addr {
		onion::service { 'ftp.debian.org':
			port => 80,
			target_port => 80,
			target_address => $onion_v4_addr,
		}
	}

	$hosts_to_check = hiera('roles.debian_mirror', {})
		.filter |$h| { $h[1]['fastly-backend'] }
		.map |$h| { $h[1]['service-hostname'] }

	roles::mirror_health { 'ftp':
		check_hosts   => $hosts_to_check,
		check_service => 'ftp',
		url           => 'http://debian.backend.mirrors.debian.org/debian/dists/sid/Release',
		health_url    => 'http://debian.backend.mirrors.debian.org/_health',
	}
}
