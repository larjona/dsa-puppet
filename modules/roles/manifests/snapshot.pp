class roles::snapshot {
	rsync::site { 'snapshot-farm':
		content => template('roles/snapshot/rsyncd.conf.erb'),
	}

	ensure_packages ( ["build-essential", "python-dev", "libssl-dev"], { ensure => 'installed' })
}
